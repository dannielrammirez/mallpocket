<?php

/* @var $this yii\web\View */

$this->title = 'Mallpocket revista';
?>
<div class="site-index">

    <div class="body-content">
		<div class="container">
			<div id="news">
				<!--- Prev noticias --->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="wrapNews">
						<a href="#">
						<img id="thumbnail" class="img-responsive" src="../../frontend/img/img_noticia.png" alt="">
						<div class="gradient">
							<h2>ARCTIC MONKEYS REGRESARÁ EN EL 2017</h2>
							<p>Leyendas del rock en una nueva producción</p>
							<div class="redesNews">
								<a target="_blank" href="https://www.facebook.com/mimallpocket?fref=ts" title="Facebook">
									<img style="width: 30px;" src="../../frontend/img/ico_facebook.png" alt="Facebook">	
								</a>

								<a target="_blank" href="https://twitter.com/mallpocket" title="Twitter">
									<img  style="width: 30px;" src="../../frontend/img/ico_tweet.png" alt="Twitter">
								</a>				
							</div>
						</div>
						</a>
					</div>						
				</div>
				<!---- /Prev noticias ---->

			   <!--- Prev noticias --->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="wrapNews">
						<a href="#">
						<img id="thumbnail" class="img-responsive" src="../../frontend/img/img_noticia2.png" alt="">
						<div class="gradient">
							<h2>PRIMERAS IMÁGENES DE LA NUEVA PELÍCULA DE DISNEY Y PIXAR</h2>
							<p>Leyendas del rock en una nueva producción</p>
							<div class="redesNews">
								<a target="_blank" href="https://www.facebook.com/mimallpocket?fref=ts" title="Facebook">
									<img style="width: 30px;" src="../../../mall2017/frontend/img/ico_facebook.png" alt="Facebook">	
								</a>

								<a target="_blank" href="https://twitter.com/mallpocket" title="Twitter">
									<img  style="width: 30px;" src="../../frontend/img/ico_tweet.png" alt="Twitter">
								</a>				
							</div>
						</div>
						</a>
					</div>						
				</div>
				<!---- /Prev noticias ---->

			   <!--- Prev noticias --->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="wrapNews">
						<a href="#">
						<img id="thumbnail" class="img-responsive" src="../../frontend/img/img_noticia3.png" alt="">
						<div class="gradient">
							<h2>LA LIGA DE LA JUSTICIA CONFIRMA A DOS PERSONAJES MÁS</h2>
							<p>Leyendas del rock en una nueva producción</p>
							<div class="redesNews">
								<a target="_blank" href="https://www.facebook.com/mimallpocket?fref=ts" title="Facebook">
									<img style="width: 30px;" src="../../frontend/img/ico_facebook.png" alt="Facebook">	
								</a>

								<a target="_blank" href="https://twitter.com/mallpocket" title="Twitter">
									<img  style="width: 30px;" src="../../frontend/img/ico_tweet.png" alt="Twitter">
								</a>				
							</div>
						</div>
						</a>
					</div>						
				</div>
				<!---- /Prev noticias ---->
			</div>
		</div> 
   
   		<div class="container">
			<div id="news">
				<!--- Prev noticias --->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="wrapNews">
						<a href="#">
						<img id="thumbnail" class="img-responsive" src="../../frontend/img/img_noticia.png" alt="">
						<div class="gradient">
							<h2>ARCTIC MONKEYS REGRESARÁ EN EL 2017</h2>
							<p>Leyendas del rock en una nueva producción</p>
							<div class="redesNews">
								<a target="_blank" href="https://www.facebook.com/mimallpocket?fref=ts" title="Facebook">
									<img style="width: 30px;" src="../../frontend/img/ico_facebook.png" alt="Facebook">	
								</a>

								<a target="_blank" href="https://twitter.com/mallpocket" title="Twitter">
									<img  style="width: 30px;" src="../../frontend/img/ico_tweet.png" alt="Twitter">
								</a>				
							</div>
						</div>
						</a>
					</div>						
				</div>
				<!---- /Prev noticias ---->

			   <!--- Prev noticias --->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="wrapNews">
						<a href="#">
						<img id="thumbnail" class="img-responsive" src="../../frontend/img/img_noticia2.png" alt="">
						<div class="gradient">
							<h2>PRIMERAS IMÁGENES DE LA NUEVA PELÍCULA DE DISNEY Y PIXAR</h2>
							<p>Leyendas del rock en una nueva producción</p>
							<div class="redesNews">
								<a target="_blank" href="https://www.facebook.com/mimallpocket?fref=ts" title="Facebook">
									<img style="width: 30px;" src="../../../mall2017/frontend/img/ico_facebook.png" alt="Facebook">	
								</a>

								<a target="_blank" href="https://twitter.com/mallpocket" title="Twitter">
									<img  style="width: 30px;" src="../../frontend/img/ico_tweet.png" alt="Twitter">
								</a>				
							</div>
						</div>
						</a>
					</div>						
				</div>
				<!---- /Prev noticias ---->

			   <!--- Prev noticias --->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="wrapNews">
						<a href="#">
						<img id="thumbnail" class="img-responsive" src="../../frontend/img/img_noticia3.png" alt="">
						<div class="gradient">
							<h2>LA LIGA DE LA JUSTICIA CONFIRMA A DOS PERSONAJES MÁS</h2>
							<p>Leyendas del rock en una nueva producción</p>
							<div class="redesNews">
								<a target="_blank" href="https://www.facebook.com/mimallpocket?fref=ts" title="Facebook">
									<img style="width: 30px;" src="../../frontend/img/ico_facebook.png" alt="Facebook">	
								</a>

								<a target="_blank" href="https://twitter.com/mallpocket" title="Twitter">
									<img  style="width: 30px;" src="../../frontend/img/ico_tweet.png" alt="Twitter">
								</a>				
							</div>
						</div>
						</a>
					</div>						
				</div>
				<!---- /Prev noticias ---->
			</div>
		</div> 
   
   		<div class="container">
			<div id="news">
				<!--- Prev noticias --->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="wrapNews">
						<a href="#">
						<img id="thumbnail" class="img-responsive" src="../../frontend/img/img_noticia.png" alt="">
						<div class="gradient">
							<h2>ARCTIC MONKEYS REGRESARÁ EN EL 2017</h2>
							<p>Leyendas del rock en una nueva producción</p>
							<div class="redesNews">
								<a target="_blank" href="https://www.facebook.com/mimallpocket?fref=ts" title="Facebook">
									<img style="width: 30px;" src="../../frontend/img/ico_facebook.png" alt="Facebook">	
								</a>

								<a target="_blank" href="https://twitter.com/mallpocket" title="Twitter">
									<img  style="width: 30px;" src="../../frontend/img/ico_tweet.png" alt="Twitter">
								</a>				
							</div>
						</div>
						</a>
					</div>						
				</div>
				<!---- /Prev noticias ---->

			   <!--- Prev noticias --->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="wrapNews">
						<a href="#">
						<img id="thumbnail" class="img-responsive" src="../../frontend/img/img_noticia2.png" alt="">
						<div class="gradient">
							<h2>PRIMERAS IMÁGENES DE LA NUEVA PELÍCULA DE DISNEY Y PIXAR</h2>
							<p>Leyendas del rock en una nueva producción</p>
							<div class="redesNews">
								<a target="_blank" href="https://www.facebook.com/mimallpocket?fref=ts" title="Facebook">
									<img style="width: 30px;" src="../../../mall2017/frontend/img/ico_facebook.png" alt="Facebook">	
								</a>

								<a target="_blank" href="https://twitter.com/mallpocket" title="Twitter">
									<img  style="width: 30px;" src="../../frontend/img/ico_tweet.png" alt="Twitter">
								</a>				
							</div>
						</div>
						</a>
					</div>						
				</div>
				<!---- /Prev noticias ---->

			   <!--- Prev noticias --->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="wrapNews">
						<a href="#">
						<img id="thumbnail" class="img-responsive" src="../../frontend/img/img_noticia3.png" alt="">
						<div class="gradient">
							<h2>LA LIGA DE LA JUSTICIA CONFIRMA A DOS PERSONAJES MÁS</h2>
							<p>Leyendas del rock en una nueva producción</p>
							<div class="redesNews">
								<a target="_blank" href="https://www.facebook.com/mimallpocket?fref=ts" title="Facebook">
									<img style="width: 30px;" src="../../frontend/img/ico_facebook.png" alt="Facebook">	
								</a>

								<a target="_blank" href="https://twitter.com/mallpocket" title="Twitter">
									<img  style="width: 30px;" src="../../frontend/img/ico_tweet.png" alt="Twitter">
								</a>				
							</div>
						</div>
						</a>
					</div>						
				</div>
				<!---- /Prev noticias ---->
			</div>
		</div> 
    </div>
</div>
