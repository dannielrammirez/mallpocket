<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="container" style="margin-top: 10px;">
	<div class="pull-left">
		<div class="logo">
			<img class="img-responsive" src="../../frontend/img/LogoMallpocket.png" alt="">
		</div>		
	</div>
	<div class="pull-right">
		<div class="submenu">
			<ul>
				<li><a href="#">REVISTA<span>secciones</span></a></li>
				<li><a href="#">EDICIÓN No41<span>Noviembre 2016 <br> GOZADERA </span></a></li>
				<li><a href="#">EL PARCHE <span>las mentes detrás<br> de mp</span></a></li>
			</ul>
			<div class="socialicons">
				<p>siguenos en:</p>
				<a target="_blank" href="https://www.facebook.com/mimallpocket?fref=ts" title="Facebook">
					<img style="width: 30px;" src="../../frontend/img/ico_facebook.png" alt="Facebook">
				</a>
				<a target="_blank" href="https://twitter.com/mallpocket" title="Twitter">
					<img  style="width: 30px;" src="../../frontend/img/ico_tweet.png" alt="Twitter">
				</a>
				<a target="_blank" href="http://instagram.com/mallpocket/" title="Instagram">
					<img  style="width: 30px;"  src="../../frontend/img/ico_insta.png" alt="Instagram">
				</a>
			</div>
			<a href="#!">
				<div class="hamburguer">
					<img class="img-responsive" src="../../frontend/img/icno_menu.png" alt="">
				</div>
				<div class="closeM">
					<img class="img-responsive" src="../../frontend/img/close.png" alt="">
				</div>
			</a>
		</div>
	</div>
</div>
	<div class="container">
	<nav class="menu">
		<ul class="Nav_New">
			<li class="" id="visible"><a href="#">ACTUALIDAD</a>
			<ul id="hidden">			
				<li><a href="#">NOTICIAS</a></li>
				<li><a href="#">EVENTOS</a></li>
				<li><a href="#">VIRALES</a></li>
			</ul>
			</li>
			<li id="visible2"><a href="#">EDITORIAL</a>
			<ul id="hidden2">			
				<li><a href="#">CRONICAS</a></li>
				<li><a href="#">REPORTAJES</a></li>
				<li><a href="#">ENTREVISTAS</a></li>
				<li><a href="#">FICCIÓN</a></li>
			</ul>
			</li>
			<li id="visible3"><a href="#">GRÁFICO</a>
			<ul id="hidden3">			
				<li><a href="#">ILUSTRACIONES</a></li>
				<li><a href="#">FOTOGRAFÍAS</a></li>
				<li><a href="#">VIDEOS</a></li>
			</ul>
			</li>
			<li id="visible4"><a href="#">EDUCACIÓN</a>
			<ul id="hidden4">			
				<li><a href="#">BECAS</a></li>
				<li><a href="#">MODA UNIVERSITARIA</a></li>
				<li><a href="#">INSCRIPCIONES</a></li>
			</ul>
			</li>
			<li id="visible5"><a class="last" href="#">BLOGS</a>
			<!-- <ul id="hidden5">			
				<li><a href="#">BECAS</a></li>
				<li><a href="#">MODA UNIVERSITARIA</a></li>
				<li><a href="#">INSCRIPCIONES</a></li>
			</ul> -->
			</li>
		</ul>	
		</nav>
	</div>
	
	
	 <div class="container">
		<!----- Variable Contenido dinamico ----->
        <?= $content ?>
        <!----- /Variable Contenido dinamico ----->
    </div>
    
    
<?php $this->endBody() ?>
	
<footer class="footer">
    <div class="container">
	<p class="center">© Copyright 2017  /  Diseño Revista Editorial Bienestar S.A.S.  /  Todos los Derechos Reservados.
	Calle 106 N° 59 - 39 Oficina 101 / 3738326 </p>
    </div>
</footer> 
</body>
</html>

<script type="text/javascript">
$(".closeM").hide();
$(".hamburguer").click(function(){
	$(this).fadeOut('');
	$(".closeM").show('');
    $("nav.menu").slideDown();
});
	
$(".closeM").click(function(){
	$(this).fadeOut();
	$(".hamburguer").show('')
	$("nav.menu").slideUp();
});
</script>
<?php $this->endPage() ?>
