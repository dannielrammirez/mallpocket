<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "galeria".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $noticia_id
 * @property integer $estado
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Noticia[] $noticias
 */
class Galeria extends \yii\db\ActiveRecord{
    const ESTADO_EN_PROCESO = 0;
    const ESTADO_CERRADO = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'galeria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'created_at', 'updated_at'], 'required'],
            [['noticia_id', 'estado', 'created_at', 'updated_at'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'noticia_id' => 'Noticia ID',
            'estado' => 'Estado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticia(){
        return $this->hasOne(Noticia::className(), ['id' => 'noticia_id']);
    }
}
