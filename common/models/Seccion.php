<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "seccion".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $estado
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property SubSeccion[] $subSeccions
 */
class Seccion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['estado', 'created_at', 'updated_at'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'estado' => 'Estado',
            'created_at' => 'Creacion',
            'updated_at' => 'Actualizacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubSeccions()
    {
        return $this->hasMany(SubSeccion::className(), ['seccion_id' => 'id']);
    }
}
