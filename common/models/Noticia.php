<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "noticia".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $nombre_imagen_banner
 * @property string $nombre_imagen_noticia
 * @property integer $sub_seccion_id
 * @property string $descripcion_corta
 * @property string $descripcion
 * @property integer $estado
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property SubSeccion $subSeccion
 * @property Galeria[] $galeria
 */
class Noticia extends \yii\db\ActiveRecord{
    public $imagenBanner;
    public $imagenNoticia;
    public $galeria;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'noticia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'sub_seccion_id', 'descripcion_corta', 'descripcion'], 'required'],
            [['imagenBanner', 'imagenNoticia'], 'file', 'extensions' => 'jpg, gif, png'],
            [['sub_seccion_id', 'estado', 'created_at', 'updated_at'], 'integer'],
            [['nombre', 'nombre_imagen_banner', 'nombre_imagen_noticia', 'descripcion_corta', 'descripcion'], 'string', 'max' => 255],
            [['sub_seccion_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubSeccion::className(), 'targetAttribute' => ['sub_seccion_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'nombre_imagen_banner' => 'Nombre Imagen Banner',
            'nombre_imagen_noticia' => 'Nombre Imagen Noticia',
            'sub_seccion_id' => 'Sub Seccion',
            'descripcion_corta' => 'Descripcion Corta',
            'descripcion' => 'Descripcion',
            'estado' => 'Estado',
            'created_at' => 'Creacion',
            'updated_at' => 'Actualizacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubSeccion(){
        return $this->hasOne(SubSeccion::className(), ['id' => 'sub_seccion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalerias(){
        return $this->hasMany(Galeria::className(), ['noticia_id' => 'id']);
    }
}
