<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "publicidad".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $nombre_imagen
 * @property string $url
 * @property integer $estado
 * @property integer $created_at
 * @property integer $updated_at
 */
class Publicidad extends \yii\db\ActiveRecord{
    public $imagen;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicidad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_imagen', 'url', 'nombre'], 'required'],
            [['imagen'], 'file', 'extensions' => 'jpg, gif, png'],
            [['estado', 'created_at', 'updated_at'], 'integer'],
            [['nombre_imagen', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_imagen' => 'Ruta Imagen',
            'nombre' => 'Nombre',
            'url' => 'Url',
            'estado' => 'Estado',
            'created_at' => 'Creacion',
            'updated_at' => 'Actualizacion',
        ];
    }
}
