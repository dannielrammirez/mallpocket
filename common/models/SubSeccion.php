<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sub_seccion".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $seccion_id
 * @property integer $estado
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Noticia[] $noticias
 * @property Seccion $seccion
 */
class SubSeccion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_seccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'seccion_id'], 'required'],
            [['seccion_id', 'estado', 'created_at', 'updated_at'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
            [['seccion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seccion::className(), 'targetAttribute' => ['seccion_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'seccion_id' => 'Seccion ID',
            'estado' => 'Estado',
            'created_at' => 'Creacion',
            'updated_at' => 'Actualizacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticias()
    {
        return $this->hasMany(Noticia::className(), ['sub_seccion_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeccion()
    {
        return $this->hasOne(Seccion::className(), ['id' => 'seccion_id']);
    }
}
