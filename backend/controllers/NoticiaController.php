<?php

namespace backend\controllers;

use Yii;
use common\models\Noticia;
use common\models\search\NoticiaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\Galeria;

/**
 * NoticiaController implements the CRUD actions for Noticia model.
 */
class NoticiaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Noticia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NoticiaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Noticia model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Noticia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new Noticia();

        if($model->load(Yii::$app->request->post())){
            $imagenBanner = UploadedFile::getInstance($model, 'imagenBanner');
            $imagenNoticia = UploadedFile::getInstance($model, 'imagenNoticia');

            $model->nombre_imagen_banner = $imagenBanner != null ? $imagenBanner->name : null;
            $model->nombre_imagen_noticia = $imagenNoticia != null ? $imagenNoticia->name : null;
            $model->created_at = time();
            $model->updated_at = time();
            if($model->save()){
                if($imagenNoticia != null)
                    $imagenNoticia->saveAs('../../images/noticia/'.$model->id.'_'.$imagenNoticia->name);
                if($imagenBanner != null)
                    $imagenBanner->saveAs('../../images/banner/'.$model->id.'_'.$imagenBanner->name);
                // Asignamos la galeria a la noticia recien creada
                Galeria::updateAll(['estado' => 1, 'noticia_id' => $model->id], 'estado = '.Galeria::ESTADO_EN_PROCESO);

                return $this->redirect(['view', 'id' => $model->id]);
            }
            else{
                echo "<pre>";
                print_r($model->getErrors());
                die();
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Noticia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Noticia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Noticia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Noticia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Noticia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpload(){
        $fileName = 'file';
        $uploadPath = '../../images/galeria';

        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);
            if($file->saveAs($uploadPath.'/'.$file->name)){
                $objGaleria = new Galeria();
                $objGaleria->nombre = $file->name;
                $objGaleria->created_at = time();
                $objGaleria->updated_at = time();
                if($objGaleria->save()){
                    echo "<pre>";
                    print_r($objGaleria->getErrors());
                    die();
                }
                echo \yii\helpers\Json::encode($file);
            }
        }

        return false;
    }
}
