<style type="text/css">
    .contentIzq h1{width: auto; float: left}
    .contentDer p{width: auto; float: right; margin-top: 20px}
</style>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Publicidad */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Publicidad', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publicidad-view">
    <div class = 'contentIzq'><h1><?= Html::encode($this->title) ?></h1></div>
    <div class = 'contentDer'>
        <p>
            <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Esta seguro que desea eliminar esta publicidad?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre_imagen',
            'url:url',
            [
                'attribute' => 'estado',
                'label' => 'Estado',
                'value' => $model->estado == 1 ? 'Activo' : 'Inactivo'
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
