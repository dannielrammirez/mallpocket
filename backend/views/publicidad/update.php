<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Publicidad */

$this->title = 'Actualiar Publicidad: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Publicidads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="publicidad-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', ['model' => $model]) ?>
</div>
