<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Seccion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seccion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    <?php
    	if(!$model->isNewRecord)
    		echo $form->field($model, 'estado')->dropDownList(['0' => 'Inactivo', '1' => 'Activo']);
    ?>

    <div class="form-group">
    </div>
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php ActiveForm::end(); ?>

</div>
