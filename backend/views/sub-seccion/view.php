<style type="text/css">
    .contentIzq h1{width: auto; float: left}
    .contentDer p{width: auto; float: right; margin-top: 20px}
</style>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SubSeccion */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Sub Seccions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-seccion-view">
    <div class = 'col-md-12'>
        <div class = 'contentIzq'><h1><?= Html::encode($this->title) ?></h1></div>
        <div class = 'contentDer'>
            <p>
                <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Esta seguro que desea eliminar esta subseccion?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        </div>
    </div>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            [
                'attribute' => 'seccion_id',
                'label' => 'Seccion',
                'value' => $model->seccion->nombre
            ],
            [
                'attribute' => 'estado',
                'label' => 'Estado',
                'value' => $model->estado == 1 ? 'Activo' : 'Inactivo'
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
