<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SubSeccion */

$this->title = 'Actualizar Sub Seccion: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Sub Seccion', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="sub-seccion-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', ['model' => $model]) ?>
</div>
