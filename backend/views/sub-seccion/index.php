<style type="text/css">
    .contentIzq h1{width: auto; float: left}
    .contentDer p{width: auto; float: right; margin-top: 20px}
</style>
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SubSeccionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Seccion';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-seccion-index">
    <div class = 'col-md-12'>
        <div class = 'contentIzq'><h1><?= Html::encode($this->title) ?></h1></div>
        <div class = 'contentDer'><p><?= Html::a('Crear Sub Seccion', ['create'], ['class' => 'btn btn-success']) ?></p></div>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            [
                'attribute' => 'seccion_id',
                'label' => 'Seccion',
                'value' => function($data){return $data->seccion->nombre;}
            ],
            [
                'attribute' => 'estado',
                'label' => 'Estado',
                'value' => function($data){return $data->estado == 1 ? 'Activo' : 'Inactivo';}
            ],
            'created_at:datetime',
            'updated_at:datetime',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
