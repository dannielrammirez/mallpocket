<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Seccion;

/* @var $this yii\web\View */
/* @var $model common\models\SubSeccion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-seccion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seccion_id')->dropdownList(ArrayHelper::map(Seccion::find()->all(), 'id', 'nombre'), ['prompt' => 'Seleccione...']); ?>

    <?php
        if(!$model->isNewRecord)
            echo $form->field($model, 'estado')->dropDownList(['0' => 'Inactivo', '1' => 'Activo']);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
