<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SubSeccion */

$this->title = 'Crear Sub Seccion';
$this->params['breadcrumbs'][] = ['label' => 'Sub Seccion', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-seccion-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', ['model' => $model]) ?>
</div>
