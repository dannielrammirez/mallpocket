<style type="text/css">
    .contentIzq h1{width: auto; float: left}
    .contentDer p{width: auto; float: right; margin-top: 20px}
</style>
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\NoticiaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticia-index">
    <div class = 'col-md-12'>
        <div class = 'contentIzq'><h1><?= Html::encode($this->title) ?></h1></div>
        <div class = 'contentDer'><p><?= Html::a('Crear Noticia', ['create'], ['class' => 'btn btn-success']) ?></p></div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'nombre_imagen_banner',
            'nombre_imagen_noticia',
            'sub_seccion_id',
            // 'descripcion_corta',
            // 'descripcion',
            // 'galeria_id',
            // 'estado',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
