<?php
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\SubSeccion;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Noticia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="noticia-form">
    <div class = 'col-md-12'>
        <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
            <div class = 'col-md-8'>
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            </div>
            <div class = 'col-md-4'>
                <?= $form->field($model, 'sub_seccion_id')->dropdownList(ArrayHelper::map(SubSeccion::find()->all(), 'id', 'nombre'), ['prompt' => 'Seleccione...']); ?>
            </div>
            <div class = 'col-md-12'>
                <?= $form->field($model, 'descripcion_corta')->textInput(['maxlength' => true]) ?>
            </div>
            <div class = 'col-md-12'>
                <?= $form->field($model, 'descripcion')->widget(CKEditor::className(), [
                    'preset' => 'full',
                    'clientOptions' => [
                        'filebrowserUploadUrl' => 'site/url'
                    ]
                ]) ?>
            </div>
            <div class = 'col-md-6'>
                <?= $form->field($model, 'imagenBanner')->widget(FileInput::classname(), [
                        'options'=>['accept'=>'image/*'],
                        'pluginOptions'=>['allowedFileExtensions'=>['jpg', 'gif', 'png']]
                    ]);
                ?>
            </div>
            <div class = 'col-md-6'>
                <?= $form->field($model, 'imagenNoticia')->widget(FileInput::classname(), [
                        'options'=>['accept'=>'image/*'],
                        'pluginOptions'=>['allowedFileExtensions'=>['jpg', 'gif', 'png']]
                    ]);
                ?>
            </div>
            <div class = 'col-md-12'>
                <?php
                    echo "<label>Galeria</label>";
                    echo \kato\DropZone::widget([
                    'options' => [
                        'url' => 'index.php?r=noticia/upload', 
                        'maxFilesize' => '2',
                        'dictDefaultMessage' => 'Arrastra los archivos aquí para subirlos',
                    ],
                    'clientEvents' => [
                        'complete' => "function(file){console.log(file)}",
                        'removedfile' => "function(file){alert(file.name + ' is removed')}"
                        ],
                    ]);
                ?>                
            </div>

            <?php
                if(!$model->isNewRecord)
                    echo $form->field($model, 'estado')->dropDownList(['0' => 'Inactivo', '1' => 'Activo']);
            ?>

            <div class = 'col-md-12'>
                <br />
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>


        <?php ActiveForm::end(); ?>
    </div>

</div>
