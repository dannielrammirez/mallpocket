<style type="text/css">
    .contentIzq h1{width: auto; float: left}
    .contentDer p{width: auto; float: right; margin-top: 20px}
</style>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Galeria;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model common\models\Noticia */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticia-view">
    <div class = 'contentIzq'><h1><?= Html::encode($this->title) ?></h1></div>
    <div class = 'contentDer'>
        <p>
            <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Esta seguro que desea eliminar esta noticia?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            [
                'attribute' => 'nombre_imagen_banner',
                'label' => 'Imagen Banner',
                'format' => 'html',
                'value' => Html::img('../../images/banner/'.$model->id.'_'.$model->nombre_imagen_banner, ['width' => '200px'])
            ],
            [
                'attribute' => 'nombre_imagen_noticia',
                'label' => 'Imagen Noticia',
                'format' => 'html',
                'value' => Html::img('../../images/noticia/'.$model->id.'_'.$model->nombre_imagen_noticia, ['width' => '200px'])
            ],
            'subSeccion.nombre',
            'descripcion_corta',
            'descripcion',
            [
                'attribute' => 'estado',
                'label' => 'Estado',
                'value' => $model->estado == 1 ? 'Activo' : 'Inactivo'
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]);

    if(count($model->galerias) > 0){
        echo "<br /><legend>Galeria</legend>";
        $data = new ActiveDataProvider(['query' => Galeria::find()->where(['noticia_id' => $model->id])]);
        echo GridView::widget([
            'dataProvider' => $data,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'nombre',
                [
                    'attribute' => 'galeria',
                    'label' => 'Galeria',
                    'format' => 'html',
                    'value' => function($obj){
                        return Html::img('../../images/galeria/'.$obj->nombre, ['width' => '200px']);
                    }
                ],
                [
                    'attribute' => 'estado',
                    'label' => 'Estado',
                    'value' => function($obj){return $obj->estado == 1 ? 'Activo' : 'Proceso';}
                ],
                'created_at:datetime',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        // $uploadPath = '../../images/galeria/';
        // echo "<legend>Galeria</legend>";
        // foreach ($model->galerias as $key => $value){
        //     echo "<img src = '".$uploadPath.$value->nombre."'/>";
        // }
    }
    ?>

</div>
