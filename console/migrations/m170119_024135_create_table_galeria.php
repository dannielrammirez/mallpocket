<?php

use yii\db\Migration;

class m170119_024135_create_table_galeria extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%galeria}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string()->null(),
            'noticia_id' => $this->integer()->notNull(),
            'estado' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_galeria_noticia_id', '{{%galeria}}', 'noticia_id', '{{%noticia}}', 'id');
        $this->addForeignKey('fk_galeria_detalle_galeria', '{{%galeria_detalle}}', 'galeria_id', '{{%galeria}}', 'id');
    }

    public function down()
    {
        echo "m170119_024135_create_table_galeria cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
