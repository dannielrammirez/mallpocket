<?php

use yii\db\Migration;

class m170114_192554_create_table_sub_seccion extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sub_seccion}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string()->notNull(),
            'seccion_id' => $this->integer()->notNull(),
            'estado' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_seccion_id', '{{%sub_seccion}}', 'seccion_id', '{{%seccion}}', 'id');
    }

    public function down()
    {
        echo "m170114_192554_create_table_sub_seccion cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
