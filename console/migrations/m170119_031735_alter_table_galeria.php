<?php

use yii\db\Migration;

class m170119_031735_alter_table_galeria extends Migration
{
    public function up(){
        $sql = 'ALTER TABLE noticia CHANGE COLUMN galeria_id galeria_id INT(5) NULL DEFAULT NULL AFTER descripcion;';
        $this->execute($sql);

        $sql0 = 'ALTER TABLE galeria 
            DROP FOREIGN KEY fk_galeria_noticia_id;
            ALTER TABLE galeria 
            DROP COLUMN noticia_id,
            DROP INDEX fk_galeria_noticia_id ;';
        $this->execute($sql0);
    }

    public function down()
    {
        echo "m170119_031735_alter_table_galeria cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
