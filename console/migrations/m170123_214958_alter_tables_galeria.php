<?php

use yii\db\Migration;

class m170123_214958_alter_tables_galeria extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE galeria CHANGE COLUMN estado estado SMALLINT(6) NOT NULL DEFAULT '0';";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170123_214958_alter_tables_galeria cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
