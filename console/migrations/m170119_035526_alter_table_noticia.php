<?php

use yii\db\Migration;

class m170119_035526_alter_table_noticia extends Migration{
    public function up(){
        $sql = "
            ALTER TABLE noticia 
CHANGE COLUMN ruta_imagen_banner nombre_imagen_banner VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL ,
CHANGE COLUMN ruta_imagen_noticia nombre_imagen_noticia VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL ;
        ";
        $this->execute($sql);
    }

    public function down(){
        echo "m170119_035526_alter_table_noticia cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
