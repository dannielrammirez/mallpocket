<?php

use yii\db\Migration;

class m170114_230302_alter_table_publicidad extends Migration
{
    public function up(){
        $sql = 'ALTER TABLE publicidad ADD COLUMN nombre VARCHAR(80) NOT NULL AFTER id;';
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170114_230302_alter_table_publicidad cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
