<?php

use yii\db\Migration;

class m170123_213448_alter_tables_galeria_galeria_detalle extends Migration
{
    public function up(){
        $sql = "
            DROP TABLE galeria_detalle;
            ALTER TABLE galeria 
            CHANGE COLUMN nombre nombre VARCHAR(255) CHARACTER SET 'utf8' NOT NULL ,
            ADD COLUMN noticia_id INT(5) NULL AFTER nombre;
        ";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170123_213448_alter_tables_galeria_galeria_detalle cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
