<?php

use yii\db\Migration;

class m170119_031419_alter_table_noticia extends Migration
{
    public function up(){
        $sql = 'ALTER TABLE noticia ADD COLUMN galeria_id INT(5) NULL AFTER updated_at;';
        $this->execute($sql);
        $this->addForeignKey('fk_noticia_galeria_id', '{{%noticia}}', 'galeria_id', '{{%galeria}}', 'id');
    }

    public function down()
    {
        echo "m170119_031419_alter_table_noticia cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
