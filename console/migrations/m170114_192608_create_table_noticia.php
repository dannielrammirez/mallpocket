<?php

use yii\db\Migration;

class m170114_192608_create_table_noticia extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%noticia}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string()->notNull(),
            'ruta_imagen_banner' => $this->string()->null(),
            'ruta_imagen_noticia' => $this->string()->null(),
            'sub_seccion_id' => $this->integer()->notNull(),
            'descripcion_corta' => $this->string()->notNull(),
            'descripcion' => $this->string()->notNull(),
            'galeria_id' => $this->integer()->null(),
            'estado' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_sub_seccion_id', '{{%noticia}}', 'sub_seccion_id', '{{%sub_seccion}}', 'id');
        $this->addForeignKey('fk_galeria_id', '{{%noticia}}', 'galeria_id', '{{%galeria}}', 'id');
    }

    public function down()
    {
        echo "m170114_192608_create_table_noticia cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
