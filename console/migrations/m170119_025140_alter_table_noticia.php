<?php

use yii\db\Migration;

class m170119_025140_alter_table_noticia extends Migration
{
    public function up(){
        $sql0 = 'ALTER TABLE noticia DROP FOREIGN KEY fk_galeria_id;';
        $sql1 = 'ALTER TABLE noticia DROP COLUMN galeria_id, DROP INDEX fk_galeria_id;';
        
        $this->execute($sql0);
        $this->execute($sql1);
    }

    public function down()
    {
        echo "m170119_025140_alter_table_noticia cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
