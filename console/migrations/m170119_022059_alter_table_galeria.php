<?php

use yii\db\Migration;

class m170119_022059_alter_table_galeria extends Migration
{
    public function up(){
        $sql = 'ALTER TABLE galeria ADD COLUMN galeria_id INT(5) NULL AFTER id, RENAME TO galeria_detalle;';
        $this->execute($sql);
    }

    public function down(){
        echo "m170119_022059_alter_table_galeria cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
