<?php

use yii\db\Migration;

class m170119_023641_alter_table_galeria_detalle extends Migration
{
    public function up(){
        $sql = 'ALTER TABLE galeria_detalle DROP COLUMN pie_de_nota;';
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170119_023641_alter_table_galeria_detalle cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
