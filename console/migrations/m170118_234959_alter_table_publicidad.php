<?php

use yii\db\Migration;

class m170118_234959_alter_table_publicidad extends Migration
{
    public function up(){
        $sql = 'ALTER TABLE publicidad CHANGE COLUMN ruta_imagen nombre_imagen VARCHAR(255) NOT NULL ;';
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170118_234959_alter_table_publicidad cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
